# Based on https://github.com/mlvlab/SageMix/blob/master/pointcloud/SageMix.py

import torch
from emd_ import emd_module

class SageMix:
    def __init__(self, num_class=4, sigma=-1, theta=0.2, device: str = "cpu"):
        self.num_class = num_class
        self.EMD = emd_module.emdModule()
        self.sigma = sigma
        self.beta = torch.distributions.beta.Beta(torch.tensor([theta]), torch.tensor([theta]))
        self.device = device
    
    def mix(self, xyz, labels, saliency=None):
        """
        Args:
            xyz (B,N,3)
            labels (B,N)
            saliency (B,N): Defaults to None.
        """        
        xyz = xyz.to(self.device)
        labels = labels.to(self.device)
        saliency = saliency.to(self.device)

        B, N, _ = xyz.shape
        idxs = torch.randperm(B)

        
        #Optimal assignment in Eq.(3)
        perm = xyz[idxs]
        
        _, ass = self.EMD(xyz, perm, 0.005, 500) # mapping
        ass = ass.long()
        perm_new = torch.zeros_like(perm).to(self.device)
        labels_new = torch.zeros_like(labels).to(self.device)
        perm_saliency = torch.zeros_like(saliency).to(self.device)
        
        for i in range(B):
            perm_new[i] = perm[i][ass[i]]
            perm_saliency[i] = saliency[idxs][i][ass[i]]
            labels_new[i] = labels[idxs][i][ass[i]]
        #####
        # Saliency-guided sequential sampling
        #####
        #Eq.(4) in the main paper
        saliency = saliency/saliency.sum(-1, keepdim=True)
        anc_idx = torch.multinomial(saliency, 1, replacement=True)
        anchor_ori = xyz[torch.arange(B), anc_idx[:,0]]
        
        #cal distance and reweighting saliency map for Eq.(5) in the main paper
        sub = perm_new - anchor_ori[:,None,:]
        dist = ((sub) ** 2).sum(2).sqrt()
        perm_saliency = perm_saliency * dist
        perm_saliency = perm_saliency/perm_saliency.sum(-1, keepdim=True)
        
        #Eq.(5) in the main paper
        anc_idx2 = torch.multinomial(perm_saliency, 1, replacement=True)
        anchor_perm = perm_new[torch.arange(B),anc_idx2[:,0]]
                
                
        #####
        # Shape-preserving continuous Mixup
        #####
        alpha = self.beta.sample((B,)).to(self.device)
        sub_ori = xyz - anchor_ori[:,None,:]
        sub_ori = ((sub_ori) ** 2).sum(2).sqrt()
        #Eq.(6) for first sample
        ker_weight_ori = torch.exp(-0.5 * (sub_ori ** 2) / (self.sigma ** 2))  #(M,N)
        
        sub_perm = perm_new - anchor_perm[:,None,:]
        sub_perm = ((sub_perm) ** 2).sum(2).sqrt()
        #Eq.(6) for second sample
        ker_weight_perm = torch.exp(-0.5 * (sub_perm ** 2) / (self.sigma ** 2))  #(M,N)
        
        #Eq.(9)
        weight_ori = ker_weight_ori * alpha 
        weight_perm = ker_weight_perm * (1-alpha)
        weight = (torch.cat([weight_ori[...,None],weight_perm[...,None]],-1)) + 1e-16
        weight = weight/weight.sum(-1)[...,None]

        #Eq.(8) for new sample
        x = weight[:,:,0:1] * xyz + weight[:,:,1:] * perm_new
        
        return x, labels_new
    